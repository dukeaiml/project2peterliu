# OpenAI Lambda Service

## Table of Contents
1. [Deliverables](#deliverables)
2. [Description](#description)
3. [Installation](#installation)
4. [Contributing](#contributing)
5. [Development Process](#development-process)

## Deliverables

![API GATEWAY INTEGRATION](/Screenshot_2024-04-24_004646.png
)


## Description

My simple AWS Lambda parses the first "api_key" variable as part of the HTTP API Gateway. It sends this as the OpenAI API key to an OpenAI HTTP Request. If there is no key or the key is invalid, the lambda defaults to a preset key which has no credits currently.

The json prompt that is entered to OpenAI API is:

"model": "gpt-3.5-turbo",
"messages": [{"role": "user", "content": "Tell me a short story of 50 words."}],
"temperature": 0.7

Which is sent.
Call my lambda function like this:
Default:
https://ewpjkfhmm4ddr77fwatw6fzhfq0myglp.lambda-url.us-east-1.on.aws/
With api_key:
https://ewpjkfhmm4ddr77fwatw6fzhfq0myglp.lambda-url.us-east-1.on.aws/?api_key=your_api_key_here 

## Installation

To run this project locally, ensure Rust, Cargo, and Docker are installed on your system. Follow these installation steps:

1. Clone the repository:
git clone https://gitlab.com/dukeaiml/project2peterliu.git
2. cd project2peterliu
3. cargo build


## Contributing

Contributions to this project are encouraged:

1. Fork the repository.
2. Create a new feature branch.
3. Commit your changes.
4. Push to the branch.
5. Create a new Pull Request.

## Development Process

Development steps included:

1. **Setup:** Establishing the Rust  framework environment.
3. **Endpoints:** Crafting routes and handlers for OpenAI operations.
6. **Testing:** Ensuring functionality through comprehensive tests.
7. **Documentation:** Crafting this detailed guide..


