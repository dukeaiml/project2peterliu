use lambda_http::{
handler,
lambda_runtime::{self, Context, Error},
IntoResponse, Request, Response, http::StatusCode, Body, RequestExt
};
use reqwest::Client;
use serde_json::json;
use std::env; // Ensure `env` module is imported

#[tokio::main]
async fn main() -> Result<(), Error> {
lambda_runtime::run(handler(my_handler)).await?;
Ok(())
}

async fn my_handler(event: Request, _ctx: Context) -> Result<impl IntoResponse, Error> {
let client = Client::new();

// Extract the API key from the query string parameters
let query_params = event.query_string_parameters();
let input_api_key = query_params.get("api_key");

// Determine which API key to use
let api_key = match input_api_key {
Some(key) if !key.is_empty() => key.to_string(),
_ => env::var("OPENAI_API_KEY").unwrap_or_default(),
};

// Check if the API key is present and valid
if api_key.is_empty() {
return Ok(Response::builder()
.status(StatusCode::UNAUTHORIZED)
.body("API key is missing or empty".into())
.unwrap());
}

let res = match client.post("https://api.openai.com/v1/chat/completions")
.header("Authorization", format!("Bearer {}", api_key))
.json(&json!({
"model": "gpt-3.5-turbo",
"messages": [{"role": "user", "content": "Tell me a short story of 50 words."}],
"temperature": 0.7
}))
.send()
.await {
Ok(response) => response,
Err(_) => return Ok(Response::builder()
.status(StatusCode::INTERNAL_SERVER_ERROR)
.body("Failed to send request".into())
.unwrap())
};

let body = match res.text().await {
Ok(text) => text,
Err(_) => return Ok(Response::builder()
.status(StatusCode::INTERNAL_SERVER_ERROR)
.body("Failed to read response body".into())
.unwrap())
};

Ok(Response::builder()
.status(StatusCode::OK)
.body(body)
.unwrap())
}